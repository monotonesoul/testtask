<?php
namespace Page;
/**
 * Class SearchResults
 * @package Page
 */
class SearchResults extends Home
{

    public static $searchButton = ['id' => '_fZl'];
    public static $resultStats = ['id'=> 'resultStats'];
    public static $firstElementInSearchResult = ['xpath' => '//div[@class=\'g\'][1]'];
    public static $orntecAsFirstInResult = [
        'xpath' =>
        '//div[@class=\'g\'][1]/descendant::a[contains(text(), \'Ortnec Services\')]'
    ];
}
