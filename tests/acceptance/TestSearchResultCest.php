<?php
namespace Acceptance;

use AcceptanceTester;
use Page\Home;
use Page\SearchResults;

/**
 * Class TestSearcResultCest
 * @package Acceptance
 */
class TestSearchResultCest
{
    /**
     * @param AcceptanceTester $I
     */
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('');
        $I->waitForElementVisible(Home::$searchInput);
    }

    /**
     * @param AcceptanceTester $I
     */
    public function testOrtnecIsFirstInSearchResult(AcceptanceTester $I)
    {
        $I->fillField(Home::$searchInput, 'Ortnec');
        $I->click(SearchResults::$searchButton);
        $I->waitForElementVisible(SearchResults::$resultStats);
        $I->seeElement(SearchResults::$orntecAsFirstInResult);
    }
}
